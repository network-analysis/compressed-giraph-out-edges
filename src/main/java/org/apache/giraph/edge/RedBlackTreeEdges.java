package org.apache.giraph.edge;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.giraph.utils.WritableUtils;
import org.apache.giraph.utils.redblacktree.RedBlackBST;
import org.apache.giraph.utils.redblacktree.RedBlackNode;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.UnmodifiableIterator;


public class RedBlackTreeEdges<I extends WritableComparable, E extends Writable>
extends ConfigurableOutEdges<I, E>
implements StrictRandomAccessOutEdges<I, E>,
MutableOutEdges<I, E> {

	RedBlackBST<I, E> tree;
	/** Number of edges. */
	private int edgeCount;
	
	@Override
	public void initialize(Iterable<Edge<I, E>> edges) {
		tree = new RedBlackBST<I, E>();
		for(Iterator<Edge<I, E>> it = edges.iterator(); it.hasNext(); ){
			Edge<I, E> next = it.next();
			tree.insert(next.getTargetVertexId(), next.getValue());
			edgeCount++;
		}
	}

	@Override
	public void initialize(int capacity) {
		// Nothing to do
		tree = new RedBlackBST<I, E>();
	}

	@Override
	public void initialize() {
		// Nothing to do
		tree = new RedBlackBST<I, E>();
	}

	@Override
	public void add(Edge<I, E> edge) {
		tree.insert(edge.getTargetVertexId(), edge.getValue());
		edgeCount++;
	}

	@Override
	public void remove(I targetVertexId) {
		tree.delete(targetVertexId);
		edgeCount--;
	}

	@Override
	public int size() {
		return edgeCount;
	}

	  /**
	   * Iterator that reuses the same Edge object.
	   */
		private class RedBlackTreeEdgeIterator extends
				UnmodifiableIterator<Edge<I, E>> {
			
			RedBlackNode<I, E> current, pre;
			ReusableEdge<I, E> representativeEdge = EdgeFactory.createReusable(null, null);
			
			/** Current edge count */
			public RedBlackTreeEdgeIterator() {
				current = tree.getRoot();
			}
			
			

	    @Override
	    public boolean hasNext() {
	      return current != null;
	    }

		@Override
	    public Edge<I, E> next() {
			  
			while (current != null) {
		            if (current.left == null) {
		            	representativeEdge.setTargetVertexId(current.key);
		            	representativeEdge.setValue(current.val);
		                current = current.right;
		                return representativeEdge;
		            } else {
		 
		                /* Find the inorder predecessor of current */
		                pre = current.left;
		                while (pre.right != null && pre.right != current) {
		                    pre = pre.right;
		                }
		 
		                /* Make current as right child of its inorder predecessor */
		                if (pre.right == null) {
		                    pre.right = current;
		                    current = current.left;
		                } 
		 
		                 /* Revert the changes made in if part to restore the original 
		                 tree i.e., fix the right child of predecssor */ else {
		                    pre.right = null;
		                    representativeEdge.setTargetVertexId(current.key);
			            	representativeEdge.setValue(current.val);
		                    current = current.right;
		                    return representativeEdge;
		                }   /* End of if condition pre->right == NULL */
		                 
		            } /* End of if condition current->left == NULL*/
		             
		        } /* End of while */
			return representativeEdge;
	    }
		
		
	  }

	  @Override
	  public Iterator<Edge<I, E>> iterator() {
	    if (edgeCount == 0) {
	      return Iterators.emptyIterator();
	    } else {
	      return new RedBlackTreeEdgeIterator();
	    }
	  }

	  
	  @Override
	  public void write(DataOutput out) throws IOException {
	    out.writeInt(edgeCount);
	    RedBlackNode<I, E> current = tree.getRoot();
		  RedBlackNode<I, E> pre;
		  while (current != null) {
	            if (current.left == null) {
	            	current.key.write(out);
	      	      	current.val.write(out);
	                current = current.right;
	            } else {
	                /* Find the inorder predecessor of current */
	                pre = current.left;
	                while (pre.right != null && pre.right != current) {
	                    pre = pre.right;
	                }
	 
	                /* Make current as right child of its inorder predecessor */
	                if (pre.right == null) {
	                    pre.right = current;
	                    current = current.left;
	                } 
	 
	                 /* Revert the changes made in if part to restore the original 
	                 tree i.e., fix the right child of predecssor */ else {
	                    pre.right = null;
		            	current.key.write(out);
		      	      	current.val.write(out);
	                    current = current.right;
	                }   /* End of if condition pre->right == NULL */
	                 
	            } /* End of if condition current->left == NULL*/
	             
	        } /* End of while */
	  }

	  @Override
	  public void readFields(DataInput in) throws IOException {
		  edgeCount = in.readInt();
		  ReusableEdge<I, E> representativeEdge = getConf().createReusableEdge();
		  initialize();
		  for (int i = 0; i < edgeCount; ++i) {
		      WritableUtils.readEdge(in, representativeEdge);
			  add(representativeEdge);
		  }
	  }
	  
	 
	@Override
	public Iterator<MutableEdge<I, E>> mutableIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E getEdgeValue(I targetVertexId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEdgeValue(I targetVertexId, E edgeValue) {
		// TODO Auto-generated method stub
		
	}

	public static void main(String[] args) {
		
		RedBlackTreeEdges<IntWritable, NullWritable> edges = new RedBlackTreeEdges<IntWritable, NullWritable>();
		
		List<Edge<IntWritable, NullWritable>> initialEdges = Lists.newArrayListWithCapacity(100);
		for (int i = 1; i < 101; i++) {
			initialEdges.add(EdgeFactory.create(new IntWritable(i)));
		}
		edges.initialize((Iterable) initialEdges);
		
		Iterator<Edge<IntWritable, NullWritable>> it = edges.iterator();
		
		for (int i = 51; i < 101; i++) {
			edges.remove(new IntWritable(i));
		}
		it = edges.iterator();
		while(it.hasNext()){
			System.out.println(it.next().getTargetVertexId().get());
		}
		
	}
	
}
