package org.apache.giraph.edge;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.giraph.utils.Trimmable;
import org.apache.giraph.utils.redblacktree.IntNullRedBlackBST;
import org.apache.giraph.utils.redblacktree.IntNullRedBlackNode;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.UnmodifiableIterator;


public class IntNullRedBlackTreeEdges extends ConfigurableOutEdges<IntWritable, NullWritable>
implements ReuseObjectsOutEdges<IntWritable, NullWritable>, Trimmable {

	IntNullRedBlackBST tree;
	/** Number of edges. */
	private int edgeCount;
	
	@Override
	public void initialize(Iterable<Edge<IntWritable, NullWritable>> edges) {
		tree = new IntNullRedBlackBST();
		for(Iterator<Edge<IntWritable, NullWritable>> it = edges.iterator(); it.hasNext(); ){
			Edge<IntWritable, NullWritable> next = it.next();
			tree.insert(next.getTargetVertexId().get());
			edgeCount++;
		}
	}

	@Override
	public void initialize(int capacity) {
		// Nothing to do
		tree = new IntNullRedBlackBST();
	}

	@Override
	public void initialize() {
		// Nothing to do
		tree = new IntNullRedBlackBST();
	}

	@Override
	public void add(Edge<IntWritable, NullWritable> edge) {
		tree.insert(edge.getTargetVertexId().get());
		edgeCount++;
	}

	@Override
	public void remove(IntWritable targetVertexId) {
		tree.delete(targetVertexId.get());
		edgeCount--;
	}

	@Override
	public int size() {
		return edgeCount;
	}

	  /**
	   * Iterator that reuses the same Edge object.
	   */
		private class IntNullRedBlackTreeEdgeIterator extends
				UnmodifiableIterator<Edge<IntWritable, NullWritable>> {
			
			IntNullRedBlackNode current, pre;
			
			/** Representative edge object. */
			  private final Edge<IntWritable, NullWritable> representativeEdge =
			            EdgeFactory.create(new IntWritable());
			/** Current edge count */
			public IntNullRedBlackTreeEdgeIterator() {
				current = tree.getRoot();
			}
			
			

	    @Override
	    public boolean hasNext() {
	      return current != null;
	    }

		@Override
	    public Edge<IntWritable, NullWritable> next() {
			  while (current != null) {
		            if (current.left == null) {
		            	representativeEdge.getTargetVertexId().set((int) current.key);
		                current = current.right;
		                return representativeEdge;
		            } else {
		 
		                /* Find the inorder predecessor of current */
		                pre = current.left;
		                while (pre.right != null && pre.right != current) {
		                    pre = pre.right;
		                }
		 
		                /* Make current as right child of its inorder predecessor */
		                if (pre.right == null) {
		                    pre.right = current;
		                    current = current.left;
		                } 
		 
		                 /* Revert the changes made in if part to restore the original 
		                 tree i.e., fix the right child of predecssor */ else {
		                    pre.right = null;
		                    representativeEdge.getTargetVertexId().set((int) current.key);
		                    current = current.right;
		                    return representativeEdge;
		                }   /* End of if condition pre->right == NULL */
		                 
		            } /* End of if condition current->left == NULL*/
		             
		        } /* End of while */
			return representativeEdge;
	    }
		
		
	  }

	  @Override
	  public Iterator<Edge<IntWritable, NullWritable>> iterator() {
	    if (edgeCount == 0) {
	      return Iterators.emptyIterator();
	    } else {
	      return new IntNullRedBlackTreeEdgeIterator();
	    }
	  }
	  @Override
	  public void write(DataOutput out) throws IOException {
		  out.writeInt(edgeCount);
		  IntNullRedBlackNode current = tree.getRoot();
		  IntNullRedBlackNode pre;
		  while (current != null) {
	            if (current.left == null) {
	            	out.writeInt((int) current.key);
	                current = current.right;
	            } else {
	                /* Find the inorder predecessor of current */
	                pre = current.left;
	                while (pre.right != null && pre.right != current) {
	                    pre = pre.right;
	                }
	 
	                /* Make current as right child of its inorder predecessor */
	                if (pre.right == null) {
	                    pre.right = current;
	                    current = current.left;
	                } 
	 
	                 /* Revert the changes made in if part to restore the original 
	                 tree i.e., fix the right child of predecssor */ else {
	                    pre.right = null;
	                    out.writeInt((int) current.key);
	                    current = current.right;
	                }   /* End of if condition pre->right == NULL */
	                 
	            } /* End of if condition current->left == NULL*/
	             
	        } /* End of while */
	  }

	  @Override
	  public void readFields(DataInput in) throws IOException {
		  final Edge<IntWritable, NullWritable> representativeEdge =
		            EdgeFactory.create(new IntWritable());
		  edgeCount = in.readInt();
		  initialize();
		  for (int i = 0; i < edgeCount; ++i) {
			  representativeEdge.getTargetVertexId().set(in.readInt());
			  add(representativeEdge);
		  }
	  }
	  
	  

	@Override
	public void trim() {
		// Nothing to do
	}
	
	

	public static void main(String[] args) {
		
		IntNullRedBlackTreeEdges edges = new IntNullRedBlackTreeEdges();
		
		List<Edge<IntWritable, NullWritable>> initialEdges = Lists.newArrayListWithCapacity(100);
		for (int i = 1; i < 101; i++) {
			initialEdges.add(EdgeFactory.create(new IntWritable(i)));
		}
		edges.initialize((Iterable) initialEdges);
		
		Iterator<Edge<IntWritable, NullWritable>> it = edges.iterator();
		
		for (int i = 51; i < 101; i++) {
			edges.remove(new IntWritable(i));
		}
		it = edges.iterator();
		while(it.hasNext()){
			System.out.println(it.next().getTargetVertexId().get());
		}
		
	}
	
	
}
