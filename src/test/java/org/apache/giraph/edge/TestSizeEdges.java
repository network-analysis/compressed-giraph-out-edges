package org.apache.giraph.edge;


import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.conf.ImmutableClassesGiraphConfiguration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.weakref.jmx.com.google.common.io.Resources;

import com.google.common.collect.Lists;


/**
 * Tests {@link TestSizeEdges} implementations.
 */
public class TestSizeEdges {

	Logger LOGGER = Logger.getLogger(TestSizeEdges.class);
	
	@Test
	public void testIndexedBitArrayEdges() {
		long startTime = System.nanoTime();
	    GiraphConfiguration giraphConfiguration = new GiraphConfiguration();
	    giraphConfiguration.setOutEdgesClass(IndexedBitArrayEdges.class);
	    ImmutableClassesGiraphConfiguration immutableClassesGiraphConfiguration = 
	    		new ImmutableClassesGiraphConfiguration(giraphConfiguration);
    	int size = 100000;
    	List<IndexedBitArrayEdges> allEdges = Lists.newArrayListWithCapacity(size);
	    try {
	    	LineIterator it = FileUtils.lineIterator(new File(Resources.getResource("uk-2007-05@100000.txt").getFile()), "UTF-8");
	    	   while (it.hasNext()) {
	    	     String line = it.nextLine();
	    	     String[] splits = line.split("\t");
	    	     IndexedBitArrayEdges edges = 
				    		(IndexedBitArrayEdges) immutableClassesGiraphConfiguration.createOutEdges();
					List<Edge<IntWritable, NullWritable>> initialEdges = Lists.newArrayListWithCapacity(splits.length-2);
					for(int i=2;i<splits.length;i++){
						initialEdges.add(EdgeFactory.create(new IntWritable(Integer.parseInt(splits[i]))));
					}
					edges.initialize((Iterable)initialEdges);
					allEdges.add(edges);
	    	   }
	    	   long endTime = System.nanoTime();
	    	   LOGGER.info("Initialization:" + (endTime - startTime) + "ns");
	    	   LOGGER.info("Size:"+ allEdges.size());
	    	   it.close();
		} catch (IOException e) {
		}
	    startTime = System.nanoTime();
	    for (IndexedBitArrayEdges edges : allEdges){
	    	for(Iterator<Edge<IntWritable, NullWritable>> edgeIter = edges.iterator(); edgeIter.hasNext();){
	    		edgeIter.next();
	    	}
	    }
	    long endTime = System.nanoTime();
	    LOGGER.info("Access:" + (endTime - startTime) + "ns");
	}
	
	@Test
	public void testCodeIntervalResidualEdges() {
		long startTime = System.nanoTime();
	    GiraphConfiguration giraphConfiguration = new GiraphConfiguration();
	    giraphConfiguration.setOutEdgesClass(CodeIntervalResidualEdges.class);
	    ImmutableClassesGiraphConfiguration immutableClassesGiraphConfiguration = 
	    		new ImmutableClassesGiraphConfiguration(giraphConfiguration);
    	int size = 100000;
    	List<CodeIntervalResidualEdges> allEdges = Lists.newArrayListWithCapacity(size);
	    try {
	    	LineIterator it = FileUtils.lineIterator(new File(Resources.getResource("uk-2007-05@100000.txt").getFile()), "UTF-8");
	    	   while (it.hasNext()) {
	    	     String line = it.nextLine();
	    	     String[] splits = line.split("\t");
	    	     CodeIntervalResidualEdges edges = 
				    		(CodeIntervalResidualEdges) immutableClassesGiraphConfiguration.createOutEdges();
					List<Edge<IntWritable, NullWritable>> initialEdges = Lists.newArrayListWithCapacity(splits.length-2);
					for(int i=2;i<splits.length;i++){
						initialEdges.add(EdgeFactory.create(new IntWritable(Integer.parseInt(splits[i]))));
					}
					edges.initialize((Iterable)initialEdges);
					allEdges.add(edges);
	    	   }
	    	   long endTime = System.nanoTime();
	    	   LOGGER.info("Initialization:" + (endTime - startTime) + "ns");
	    	   LOGGER.info("Size:"+ allEdges.size());
	    	   it.close();
		} catch (IOException e) {
		}
	    startTime = System.nanoTime();
	    for (CodeIntervalResidualEdges edges : allEdges){
	    	for(Iterator<Edge<IntWritable, NullWritable>> edgeIter = edges.iterator(); edgeIter.hasNext();){
	    		edgeIter.next();
	    	}
	    }
	    long endTime = System.nanoTime();
	    LOGGER.info("Access:" + (endTime - startTime) + "ns");
	}
	
	@Test
	public void testIntervalResidualEdges() {
		long startTime = System.nanoTime();
	    GiraphConfiguration giraphConfiguration = new GiraphConfiguration();
	    giraphConfiguration.setOutEdgesClass(IntervalResidualEdges.class);
	    ImmutableClassesGiraphConfiguration immutableClassesGiraphConfiguration = 
	    		new ImmutableClassesGiraphConfiguration(giraphConfiguration);
    	int size = 100000;
    	List<IntervalResidualEdges> allEdges = Lists.newArrayListWithCapacity(size);
	    try {
	    	LineIterator it = FileUtils.lineIterator(new File(Resources.getResource("uk-2007-05@100000.txt").getFile()), "UTF-8");
	    	   while (it.hasNext()) {
	    	     String line = it.nextLine();
	    	     String[] splits = line.split("\t");
	    	     IntervalResidualEdges edges = 
				    		(IntervalResidualEdges) immutableClassesGiraphConfiguration.createOutEdges();
					List<Edge<IntWritable, NullWritable>> initialEdges = Lists.newArrayListWithCapacity(splits.length-2);
					for(int i=2;i<splits.length;i++){
						initialEdges.add(EdgeFactory.create(new IntWritable(Integer.parseInt(splits[i]))));
					}
					edges.initialize((Iterable)initialEdges);
					allEdges.add(edges);
	    	   }
	    	   long endTime = System.nanoTime();
	    	   LOGGER.info("Initialization:" + (endTime - startTime) + "ns");
	    	   LOGGER.info("Size:"+ allEdges.size());
	    	   it.close();
		} catch (IOException e) {
		}
	    startTime = System.nanoTime();
	    for (IntervalResidualEdges edges : allEdges){
	    	for(Iterator<Edge<IntWritable, NullWritable>> edgeIter = edges.iterator(); edgeIter.hasNext();){
	    		edgeIter.next();
	    	}
	    }
	    long endTime = System.nanoTime();
	    LOGGER.info("Access:" + (endTime - startTime) + "ns");
	}
}
