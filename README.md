Memory-Optimized Distributed Graph Processing through Novel Compression Techniques
=====

The implementation of the out-edges representations proposed in: P. Liakos, K. Papakonstantinopoulou, A. Delis. Memory-Optimized Distributed Graph Processing through Novel Compression Techniques. CIKM 2016, Indianapolis, USA.

The graph [uk-2007-05@100000.txt](http://law.di.unimi.it/webdata/uk-2007-05@100000/) from [LAW](http://law.di.unimi.it/datasets.php) is included for testing purposes. To perform experiments using our implementation you can follow the guide [here](http://giraph.apache.org/quick_start.html) to setup the environment, upload graph uk-2007-05@100000.txt to your HDFS and then execute the following:


> $HADOOP_HOME/bin/hadoop jar $GIRAPH_HOME/giraph-examples/target/giraph-examples-1.1.0-for-hadoop-1.2.1-jar-with-dependencies.jar org.apache.giraph.GiraphRunner org.apache.giraph.examples.SimpleIntPageRankComputation -vif org.apache.giraph.io.formats.IntDoubleNullTextInputFormat -vip /user/hduser/input/uk-2007-05@100000.txt -vof org.apache.giraph.io.formats.IdWithValueTextOutputFormat -op /user/hduser/output/out -w 2 -mc org.apache.giraph.examples.SimpleIntPageRankComputation\$SimplePageRankMasterCompute -ve org.apache.giraph.edge.IntervalResidualEdges

Similarly, you can define org.apache.giraph.edge.CodeIntervalResidualEdges or org.apache.giraph.edge.IndexedBitArrayEdges as your desired representation.

Remember to setup the $HADOOP_CLASSPATH environment variable as follows (adjust the file paths accordingly):

> export HADOOP_CLASSPATH=/usr/local/giraph/lib/giraph-examples-1.1.0-for-hadoop-1.2.1-jar-with-dependencies.jar:/usr/local/giraph/lib/compressed-giraph-out-edges-1.0.0.jar:/usr/local/giraph/lib/dsiutils-2.2.3.jar:/usr/local/giraph/lib/webgraph-3.4.3.jar


Visit our [group](http://hive.di.uoa.gr/network-analysis) for more details.